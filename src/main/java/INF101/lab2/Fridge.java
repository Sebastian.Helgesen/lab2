package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    public List<FridgeItem> items = new ArrayList<>();

    public final Integer maxsize = 20;

    @Override
    public int nItemsInFridge() {
    return this.items.size();
    }

    @Override
    public int totalSize() {
        return this.maxsize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(this.items.size()<this.maxsize) {
            this.items.add(item);
            return true;
        }else {
            return false;
        }

    }

    @Override
    public void takeOut(FridgeItem item) {
        if(this.items.contains(item)) {
            this.items.remove(item);
        }else {
            throw new NoSuchElementException();
        }

    }

    @Override
    public void emptyFridge() {
        this.items.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();
        for(FridgeItem item: this.items) {
            try{
                if(item.hasExpired()){
                    expiredItems.add(item);
                }

            }catch (IllegalStateException e) {
                expiredItems.add(item);

            }

        }
        for(FridgeItem item : expiredItems) {
            this.takeOut(item);
        }
        return expiredItems;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(FridgeItem item : this.items) {
            sb.append(item.toString()).append("\n");
        }
        return sb.toString();
    }

}
